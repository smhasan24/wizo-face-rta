import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
import { emoImage } from '../api/tasks.js';
import { Commands } from '../api/tasks.js';
import { Faqs } from '../api/tasks.js';
import { Script } from '../api/tasks.js';


import './body.html';

Session.setDefault('isTrue', false);
Session.setDefault('pop', false);
Session.setDefault('talk', false);
Session.setDefault('standby', false);

Meteor.startup(function () {
  //console.log(emoImage.findOne({_id:0}));
  if(Meteor.isCordova){
    StatusBar.show();
    StatusBar.styleDefault();
    StatusBar.backgroundColorByName("black");
  }
});

/////////////////////Main Body/////////////////////////////////////
Template.body.helpers({

  isTrue(){
    console.log("helped");
    console.log(Session.get('isTrue'));
    return Session.get('isTrue');
  },
  pop(){
    return Session.get('pop');
  },
  Faq(){

    if (Commands.findOne({_id:1}).condition == 1)
      {Session.set ('pop', true)}
    else if (Commands.findOne({_id:1}).condition != 1)
      {Session.set ('pop', false)}
    console.log("pop: " + Session.get('pop'));
    if(Session.get('pop') == true)
    {
      return "/images/blackBox.png";
    }
    else
    {
      return "/images/FAQ.png";
    }
  },
  Mic(){
    if(Commands.findOne({_id:1}).standby){
      return "/images/micStandby.png"
    }
    else if (Session.get('talk')){
      return "/images/micOn.gif"
    }
    else{
      return "/images/micOff.png"
    }
  }
});

Template.body.events({
 'click .faq-button'(event) {
    Session.set('isTrue', !Session.get('isTrue'));
 },
 'click .mic-button'(event){
    Session.set('talk', !Session.get('talk'));
    Commands.update({_id:1},{$set : {standby:false}});
    Commands.update({_id:1},{$set : {talkStatus:Session.get('talk')}});
    console.log("talk talkStatus" + Commands.findOne({_id:1}).talkStatus);
 },
});

///////////////////FAQ////////////////////////////////////////////
Template.faq.helpers({
  FAQ() {
    return Faqs.find();
  }
});
Template.faq.events({
  'click .questions'(event) {
    console.log( Faqs.findOne({question:this.question}).answer);
    Script.update({"_id":1}, {$set:{wizo:Faqs.findOne({question:this.question}).answer}});
    Session.set('isTrue', !Session.get('isTrue'));
  }
});
/////////////////FACE/////////////////////////////////////////////
Template.face.helpers({

  textLimitW(){
    var limitReached = true;
    var content = Script.findOne({_id: 1}).wizo;
    var contentArray = content.split(" "),
            current = 0,
            elem = "";
    var myVar = setInterval(function() {
          console.log("interval");
            if(current < contentArray.length) {
                if (elem.length<100){
                  console.log("adding");
                  elem = elem + contentArray[current++] +  " "
                  if (current == elem.length){clearInterval(myVar);}
                }
                else{
                  current = 0
                  if (limitReached){
                    elem = elem + "...";
                    limitReached = false;
                    console.log(elem);
                    clearInterval(myVar);
                  }
                }
                console.log(current);
                Script.update({_id:2},{$set : {textUpdated:elem}});
            }
        }, 200);
        elem = ""
        return "";

  },
  wizo() {

    return Script.findOne({_id: 2}).textUpdated;
    //return elem;
  },
  eyes()  {
    var emo = Commands.findOne({_id:1}).emotion;
    console.log(emo);
    if (emo > 4){emo = 0}
    var location = Commands.findOne({el:emo});
    console.log(location);
    return Commands.findOne({el:emo}).url

    // if (Commands.findOne({_id:1}).emotion == 0){
    //   return "/images/eyes/neutralEye.gif"
    // }
    // else if (Commands.findOne({_id:1}).emotion == 1){
    //   return "/images/eyes/fearEye.gif"
    // }
    // else if (Commands.findOne({_id:1}).emotion == 2){
    //   return "/images/eyes/happyEye.gif"
    // }
    // else if (Commands.findOne({_id:1}).emotion == 3){
    //   return "/images/eyes/surprisedEye.gif"
    // }
    // else if (Commands.findOne({_id:1}).emotion == 4){
    //   return "/images/eyes/angryEye.gif"
    // }
  }
});
/////////////////POPUP////////////////////////////////////////////
Template.popUp.helpers({

  popMessage(){
    return Commands.findOne({_id:1}).message;
  }

});

Template.popUp.events({
  'click .yes'(event) {



    console.log("yes");
    Commands.update({_id:1},{$set : {condition:2}})
  },

  'click .no'(event) {



    console.log("no");
    Commands.update({_id:1},{$set : {condition:3}})
  }
});
